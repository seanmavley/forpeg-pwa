import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  user: any;

  constructor(public afAuth: AngularFireAuth, private router: Router) {}
  ngOnInit() {
    this.afAuth.authState.subscribe(res => {
      this.user = res;
    });
  }
  logout() {
    this.afAuth.auth.signOut();
    this.router.navigate(['/auth']);
  }
}
