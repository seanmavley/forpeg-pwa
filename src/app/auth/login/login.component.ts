import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  error: any;
  busy: boolean;

  constructor(public afAuth: AngularFireAuth, private router: Router) {}
  login() {
    this.busy = true;
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider())
      .then((res) => {
        this.busy = false;
        if (!res.user) {
          this.error = 'Error';
          return;
        }
        this.router.navigate(['/']);
      })
      .catch((error) => {
        this.busy = false;
        this.error = error.message;
      });
  }

  ngOnInit() {}
}
