import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  busy: boolean;
  public customers: any;
  private customerCollection: AngularFirestoreCollection;

  constructor(private afs: AngularFirestore, private afAuth: AngularFireAuth) { }

  ngOnInit() {
    this.busy = true;
    const user_id = this.afAuth.auth.currentUser.uid;
    this.customerCollection = this.afs.collection(`customer/${user_id}/customers`);
    this.customerCollection.valueChanges()
      .subscribe(res => {
        console.log(res);
        this.busy = false;
        this.customers = res;
      });
  }

}
