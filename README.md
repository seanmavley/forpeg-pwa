# ForPeg

This project is based on specifications for a job interview assignment.

To view this project live, visit [forPeg](https://peg.khophi.tech)

This is an accompanying PWA (Progressive Web App) version of the [mobile app](https://drive.google.com/drive/folders/1xL3ZZ2Z05uuzbyxuRUpa8w7REwF-LqFO?usp=sharing).

This PWA offers more advantages over the conventional [mobile app](https://drive.google.com/drive/folders/1xL3ZZ2Z05uuzbyxuRUpa8w7REwF-LqFO?usp=sharing).

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version

## Tech Stack

This project uses the following tech stack:

 - Angular Frontend Framework
 - Paper CSS Framework
 - Firebase (Auth & Firestore)

And this project follows the principles of Progressive Web App

## Development server

To run locally, 

 - clone this repository
 - run `npm install` in the project root directory
 - then run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.